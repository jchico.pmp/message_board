const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new Schema({
	firstName : {
		type: String,
			required: [true, "First Name field required"]
			},
	lastName : {
		type: String,
			required: [true, "Last Name field required"]
			},	
	});


module.exports = mongoose.model('User', UserSchema);
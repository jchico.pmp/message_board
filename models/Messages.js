const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const MessageSchema = new Schema({
	title : {
		type: String,
			required: [true, "Title field required"]
			},
	description : {
		type: String,
			required: [true, "Description field required"]
			},
	likes : {
		type: Number,
			required: [true, "Likes field required"]
			},
	author : {
		type: String,
			required: [true, "Author field required"]
			}					
	});


module.exports = mongoose.model('Message', MessageSchema);
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


//initialize the app
const app = express();
const port = 3001;

//connect to database
mongoose.connect(
	'mongodb://localhost:27017/Message_Board',
	 {
	 	useNewUrlParser: true,
	 	useUnifiedTopology: true,
	 	
	 });
mongoose.connect('connected', () => {
	console.log("Ikaw ay nakakonekta na sa iyong database.");
})

//dependencies
app.use(bodyParser.json());
app.use((req, res, next) => {
	console.log(req.body)
	next();
})


//route
app.use('/users', require('./routes/users.js'));
app.use('/messages', require('./routes/messages.js'));





//error handling
app.use(function(err, req, res, next) {
	res.status(422).json({
		error : err.message
	})
})



//port
app.listen(port, () => {
	console.log(`Listening to port ${port}`);
});









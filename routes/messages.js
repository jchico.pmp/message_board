const express = require('express');
const router = express.Router();
const MessageModel = require('../models/Messages.js');

//index
router.get('/', (req, res, next) => {

	MessageModel.find().then(
		(messages) => {
			res.json(messages)
		}).catch(next)
});


//create
router.post('/', (req, res, next) => {
	MessageModel.create({
		title : req.body.title,
		description : req.body.description,
		likes : req.body.likes,
		author : req.body.author
	}).then( (messages) => {
		res.send(messages)
	}).catch(next)
});

//single show
router.get('/:id', (req, res, next) => {
	MessageModel.findOne({
		_id : req.params.id
	}).then( (messages) => {
		res.json(messages)
	}).catch(next)
});

//update
router.put('/:id', (req, res, next) => {
	MessageModel.findOneAndUpdate({
		_id : req.params.id
	},
	{
		title : req.body.title,
		description : req.body.description,
		likes : req.body.likes,
		author : req.body.author
	},
	{
		new : true
	}
	).then( (messages) => {
		res.json(messages)
	}).catch(next)
});

//delete
router.delete('/:id', (req, res, next) => {
	MessageModel.findOneAndDelete({
		_id : req.params.id
	}).then( (messages) => {
		res.json(messages)
	}).catch(next)
});


//export
module.exports = router;
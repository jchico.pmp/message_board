const express = require('express');
const router = express.Router();
const UserModel = require('../models/Users.js');

//index
router.get('/', (req, res, next) => {

	UserModel.find().then(
		(users) => {
			res.json(users)
		}).catch(next)
});


//create
router.post('/', (req, res, next) => {
	UserModel.create({
		firstName : req.body.firstName,
		lastName : req.body.lastName
	}).then( (users) => {
		res.send(users)
	}).catch(next)
});

//single show
router.get('/:id', (req, res, next) => {
	UserModel.findOne({
		_id : req.params.id
	}).then( (users) => {
		res.json(users)
	}).catch(next)
});

//update
router.put('/:id', (req, res, next) => {
	UserModel.findOneAndUpdate({
		_id : req.params.id
	},
	{
		firstName : req.body.firstName,
		lastName : req.body.lastName
	},
	{
		new : true
	}
	).then( (users) => {
		res.json(users)
	}).catch(next)
});

//delete
router.delete('/:id', (req, res, next) => {
	UserModel.findOneAndDelete({
		_id : req.params.id
	}).then( (users) => {
		res.json(users)
	}).catch(next)
});


//export
module.exports = router;